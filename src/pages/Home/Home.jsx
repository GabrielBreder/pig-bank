import React from 'react';
import { useNavigate } from 'react-router-dom';

import classes from './Home.module.css';

import Button from '../../components/Button/Button';
import Title from '../../components/Title/Title';
import DetailImage from '../../components/DetailImage/DetailImage';

import moneyPig from '../../assets/img/MoneyPig.png'
import incomeImg from '../../assets/img/Income.png';

function Home() {

  const navigate = useNavigate();

  function navigateToOpenAccount() {
    navigate("/building");
  }

  function navigateToCoinPrice() {
    navigate("/coin-price");
  }

  return (
    <>
      <section className={classes.openAccount_container}>
        <div className={classes.img_container}>
          <img src={moneyPig} alt="" />
        </div>
        <div className={classes.openAccount_text}>
          <h2>A mais nova alternativa de banco digital chegou!</h2>
          <p>Feito para caber no seu bolso e otimizar seu tempo. O <b>PigBank</b> veio pra ficar</p>
          <Button 
            text="Abra sua conta"
            onclick={navigateToOpenAccount}
          />
        </div>
      </section>
      <Title text="Fique por dentro!" />
      <section className={classes.coinPrice_container}>
  
          <p>Ao contrário do ditado popular, por aqui quem se mistura com porco não come farelo! Conheça nossa plataforma exclusivamente dedicada para ampliar o <b>seu</b> patrimônio.</p>
          <img src={incomeImg} alt="" />
          <Button 
            text="Cotação Moedas"
            onclick={navigateToCoinPrice}
          />
    
      </section>
      <DetailImage />
    </>
  );
}

export default Home;