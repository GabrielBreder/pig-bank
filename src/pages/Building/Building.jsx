import React from 'react';
import BlankPage from '../../components/BlankPage/BlankPage';

import buildingImg from '../../assets/img/Building.png'

function Building() {
  return (
    <BlankPage 
      title="Em construção" 
      image={buildingImg} 
      infoText_firstLine="Parece que essa página ainda não foi implementada..." 
      infoText_secondLine=" Tente novamente mais tarde!"
    />
  );
}

export default Building;