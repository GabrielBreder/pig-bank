import React from 'react';

import BlankPage from '../../components/BlankPage/BlankPage';
import errorPig from '../../assets/img/ErrorPig.png'

function NotFound() {
  return (
    <BlankPage 
      title="Em construção" 
      image={errorPig} 
      infoText_firstLine="Ops! Parece que o endereço que você digitou está incorreto..." 
    />
  );
}

export default NotFound;