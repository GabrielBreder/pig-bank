import React, { useContext, useEffect, useState } from 'react';

// import { Container } from './styles';

import Title from '../../components/Title/Title'
import CoinCard from '../../components/CoinCard/CoinCard'
import DetailImage from '../../components/DetailImage/DetailImage'

import { CoinContext } from '../../context/CoinContext'

function CoinPrice() {

  const context = useContext(CoinContext)
  
  useEffect(() => {
    context.getCoins()
  }, [])

  const coins = context.coins

  return (
    <>
      <Title text="Cotação Moedas" />
      {coins.map(coin => {

        let date = coin.create_date
        date = date.split(' ')

        let year = date[0]
        let time = date[1]

        year = year.split('-').reverse().join('/')

        let name = coin.name.split('/')[0]

        return (
          <CoinCard 
            code={coin.code}
            name={name}
            high={coin.high}
            low={coin.low}
            year={year}
            time={time}
          />
        )
      })}
    
    
      <DetailImage />
    </>
  );
}

export default CoinPrice;