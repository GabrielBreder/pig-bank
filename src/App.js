import './App.css';
import { CoinProvider } from './context/CoinContext';

import { AppRoutes } from './routes/Routes';

function App() {
  return (
    <CoinProvider>
      <AppRoutes />
    </CoinProvider>
  );
}

export default App;
