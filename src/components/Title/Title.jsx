import React from 'react';

import classes from './Title.module.css';

function Title({text}) {
  return (
    <section className={classes.title_container}>
      <div className={classes.rectangle}></div>
      <h1 className={classes.title}>{text}</h1>
      <div className={classes.rectangle}></div>
    </section>
  );
}

export default Title;