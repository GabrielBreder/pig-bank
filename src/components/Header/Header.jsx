import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from '../Button/Button';

import classes from './Header.module.css';

function Header() {

  const navigate = useNavigate()

  function navigateToCoinPrice() {
    navigate("/coin-price")
  }

  function navigateToAccess() {
    navigate("/building")
  }

  return (
    <header className={classes.header}>
      <Link to="/">
        <h2 className={classes.logo}>Pigbank</h2>
      </Link>
      <ul className={classes.buttons_list} >
        <Button text="Cotação Moedas" btnSize="small" onclick={navigateToCoinPrice} />
        <Button text="Acessar" btnColor="white" btnSize="small" onclick={navigateToAccess} />
      </ul>
    </header>
  );
}

export default Header;