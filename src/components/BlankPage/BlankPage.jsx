import React from 'react';

import classes from './BlankPage.module.css';

import Button from '../Button/Button'
import { useNavigate } from 'react-router-dom';

function BlankPage({title, image, infoText_firstLine, infoText_secondLine}) {
  
  const navigate = useNavigate()

  function navigateBack() {
    navigate(-1)
  }


  return (
    <section className={classes.blankPage_container}>
      <h2 className={classes.title}>{title}</h2>
      <img src={image} alt="" />
      <div className={classes.infoText}>
        <p>{infoText_firstLine}</p>
        <p>{infoText_secondLine}</p>
      </div>
      <Button 
        text="VOLTAR" 
        btnSize="small" 
        onclick={navigateBack}
      />
    </section>
  );
}

export default BlankPage;