import React from 'react';

import classes from './Footer.module.css';

function Footer() {
  return (
    <footer className={classes.footer}>
      <h3>FEITO COM AMOR</h3>
    </footer>
  );
}

export default Footer;