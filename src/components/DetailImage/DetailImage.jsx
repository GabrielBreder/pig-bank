import React from 'react';

import classes from './DetailImage.module.css';
import circles from '../../assets/img/circles.png';

function DetailImage() {
  return (
    <div className={classes.container}>
      <img src={circles} alt="" />
    </div>  
  );
}

export default DetailImage;