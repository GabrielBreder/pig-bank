import React from 'react';

import classes from './CoinCard.module.css';

function CoinCard({code, name, high, low, year, time}) {
  return (
    <div className={classes.card_container}>
      <div className={classes.coinTitle_container}>
        <h4>{name}</h4>
        <h1>{code}</h1>
      </div>
      <div className={classes.division}></div>
      <div className={classes.coinInfo_container}>
        <h3>Máxima: R$ {high}</h3>
        <h3>Mínima: R$ {low}</h3>
        <p>Atualizado em {year} às {time}</p>
      </div>
    </div>
  );
}

export default CoinCard;