import React from 'react';

import classes from './Button.module.css';

function Button({text, btnColor, btnSize, onclick}) {

  let backgroundColor = '#f6949e'
  let color = 'white'
  let width = '270px';
  let height = '50px';
  let fontSize = '1.5rem';
 

  if (btnColor === 'white') {
    backgroundColor = 'white';
    color = '#f6949e';
  }

  if (btnSize === 'small') {
    width = '150px';
    height = '35px';
    fontSize = '1rem';
  }
  
  const style = {
    backgroundColor,
    color,
    width,
    height,
    fontSize
  }

  return (
    <button 
      className={classes.button}
      style={style}
      onClick={onclick}
    >
      {text}
    </button>
  );
}

export default Button;