import { useState } from 'react'
import api from '../../services/api'

export function useCoin() {

  const [coins, setCoins] = useState([])

  function getCoins() {
    api.get()
    .then(res => {
      const data = res.data
     console.log(data)
      Object.keys(data).map(function(key, index) {
        setCoins(coins => [...coins, data[key]]);
      });
    })
    .catch(err => console.log(err))
  }




  return {getCoins, coins}
}