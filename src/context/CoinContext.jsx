import { createContext } from "react";
import { useCoin } from "./hooks/useCoin";

const CoinContext = createContext()

function CoinProvider( { children } ) {
  const { getCoins, coins } = useCoin()

  return (
    <CoinContext.Provider value={{getCoins, coins}}>
      { children }
    </CoinContext.Provider>
  )
}

export {CoinContext, CoinProvider}