import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";

import Home from '../pages/Home/Home'
import CoinPrice from '../pages/CoinPrice/CoinPrice'
import Building from '../pages/Building/Building'
import NotFound from '../pages/NotFound/NotFound'

export function AppRoutes() {
  return (
    <Router>
      <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/coin-price" element={<CoinPrice />} />
          <Route path="/building" element={<Building />} />

          <Route path="*" element={<NotFound />} />
        </Routes>
      <Footer />
    </Router>
  );
}